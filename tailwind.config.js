module.exports = {
    content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
    // darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors:{
                'color-btn' : '#404040',
                'color-error' : '#FF0202',
                'bg-input' : '#EDF2F7',
                'color-text-default' : '#333333',
                'default-bg' : '#404040',
                'default-color' : '#404040',
                'default-color-light':'#767676',
                'default-color-medium':'#5F5F5F',
                'btn-detail-class' : '#929292',
                'btn-detail-period' : '#5F5F5F',
                'gray-01' : '#CBCBCB',
                'gray-02' : '#EEEEEE',
                'blue-01' : '#0000FF',
                'black-01' : '#404041',
                'gray-03' : '#3F4254',
                'gray-04' :'#979797',
                'gray-05' :'#9E9E9E',
                'gray-06': '#C4C4C4',
                'gray-07': '#B5191A',
               

            },
           
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}