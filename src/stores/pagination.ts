import {defineStore} from "pinia";
// @ts-ignore
import {API} from "@/utils/constant";
// @ts-ignore
import axiosConfig from "@/services/api";


export const usePaginationStore = defineStore("pagination", {
    state: () => {
        return {
            page: 1
        };
    },
    actions: {
        setCurrentPage(page: any) {
            this.page = page
        },
        changePageDefault(listPage: any, currentPage: any) {
            if (currentPage > 1 && listPage.length === 1) {
                return currentPage - 1;
            } else {
                return currentPage;
            }
        }
    },
});
