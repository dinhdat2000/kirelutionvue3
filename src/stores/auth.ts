import {defineStore} from 'pinia';
// @ts-ignore
import JwtService, {destroyToken} from "@/services/jwt.service";
// @ts-ignore
import axiosConfig from "@/services/api.js"

export const LOGIN = '/login';
export const SEND_EMAIL = '/auth/send-mail';
export const UPDATE_PASSWORD = '/auth/reset-password';
export const CHANGE_PASSWORD = '/update-password';
export const CHANGE_PASSWORD_USER = '/admin/admin/change-password';
export const GET_DETAIL_USER = '/auth/detail-user';

interface Auth {
    is_authenticated: boolean;
    user: any;
    is_active: boolean,
    user_id: number,
    role: number,
    error: any
}

export const authStore = defineStore('auth', {
    state: (): Auth => {
        return {
            user: null,
            is_authenticated: false,
            is_active: false,
            user_id: 0,
            role: 0,
            error: ''
        };
    },
    actions: {
        async login(payload: any, disableToastError) {
            return await axiosConfig.post(LOGIN, payload, {
                disableToastError: disableToastError
            }).then((data: any) => {
                this.is_authenticated = true;
                if (data.status == 0 && data.code) {
                    this.error = data.code
                }
                this.user = data.data.data
                JwtService.saveToken(data.data.access_token)
                return data;
            })
        },
        async sendEmail(payload: any) {
            const data = await axiosConfig.post(SEND_EMAIL, payload)
            return data;
        },
        async updatePassword(payload: any) {
            const data = await axiosConfig.post(UPDATE_PASSWORD, payload)
            return data
        },
        async changePassword(payload: any) {
            const data = await axiosConfig.post(CHANGE_PASSWORD, payload);
            return data;
        },
        setUserID(id: number) {
            this.user_id = id
        },
        async changePasswordUser(payload: any) {
            const data = await axiosConfig.post(CHANGE_PASSWORD_USER, payload)
            return data;
        },
        async getDetailUser() {
            await axiosConfig.get(GET_DETAIL_USER).then(({data}: any) => {
                this.user = data
                if(data && data.status == 0) {
                    JwtService.destroyToken();
                    window.location.href = '/login';
                }
            })
        },
    },
});
