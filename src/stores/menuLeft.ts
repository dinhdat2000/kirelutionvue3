import {defineStore} from "pinia";
import {useI18n} from "vue-i18n";

interface MenuItem {
    title: string;
    src: string;
    type: string;
    icon :string;
    color?: string;
    name: string | string[];
    isShowChild?: boolean;
    active: boolean;
    child?: any;
    role: any;
    iconColor?: string;
}

export const useMenuLeftStore = defineStore("menuLeft", {
    state: () => {
        return {
            listMenuLeft: [] as MenuItem[],
        };
    },
    actions: {
        getMenuLeft(roleId: any) {
            this.setMenuLeft();
            this.listMenuLeft = this.listMenuLeft.filter(item => {
                if (item.role && item.role.includes(roleId)) {
                    if (item.child && item.child.length > 0) {
                        item.child = item.child.filter(itemChild => {
                            if (itemChild.role && itemChild.role.includes(roleId)) {
                                return this.listMenuLeft;
                            }
                        });
                    }
                    return this.listMenuLeft;
                }

            });
        },

        setMenuLeft() {
            const {t} = useI18n();
            this.listMenuLeft = [
                {
                    title: t("menu.default.dashboard"),
                    src: "/",
                    icon:"home",
                    type: "dashboard",
                    name: "Home",
                    active: true,
                    role: [0],
                },
                {
                    title: t("menu.default.introduction"),
                    src: "/introduction",
                    type: "text",
                    icon: "text",
                    name: "introduction",
                    active: true,
                    role: [0],
                },
                {
                    title: t("menu.default.ceo"),
                    src: "/ceo",
                    type: "text",
                    icon:"text",
                    name: "ceo",
                    active: true,
                    role: [0],
                },
                {
                    title: t("menu.default.what_kire"),
                    src: "/whats_kirei",
                    type: "whatskirei",
                    icon:"whatskirei",
                    name: "whatskirei",
                    active: true,
                    role: [0]
                },
                {
                    title: t("menu.default.service"),
                    src: "",
                    type: "service",
                    icon:"service",
                    name: "",
                    active: true,
                    role: [0],
                    isShowChild: true,
                    child: [
                        {
                            title: t("menu.default.banner_service"),
                            type: "ellipse",
                            icon:"ellipse",
                            active: false,
                            name: ["banner_service"],
                            src: "/service/banner_service",
                            role: [0, 1]
                        },
                        {
                            title: t("category.category_service").toString(),
                            type: "ellipse",
                            icon:"ellipse",
                            active: false,
                            name: "category_service",
                            src: "/service/category_service",
                            role: [0, 1]
                        },
                        {
                            title: t("menu.default.list_service").toString(),
                            type: "ellipse",
                            icon:"ellipse",
                            active: true,
                            name: "list-service",
                            src: "/service/list_service",
                            role: [0, 1]
                        },
                    ]
                },
                {
                    title: t("menu.default.team_kire"),
                    src: "/team_kirei",
                    icon:"team_kire",
                    type: "team_kire",
                    name: "team_kirei",
                    active: true,
                    role: [0]
                },
                {
                    title: t("menu.default.kirei_plus").toString(),
                    src: "",
                    type: "kireiplus",
                    icon:"kireiplus",
                    name: "kireiplus",
                    active: true,
                    role: [0],
                    isShowChild: true,
                    child: [
                        {
                            title: t("menu.default.banner_hastag").toString(),
                            type: "ellipse",
                            icon:"ellipse",
                            active: false,
                            name: ["BannerHastag"],
                            src: "/plus/banner_hastag",
                            role: [0, 1]
                        },
                        {
                            title: t("category.category_news").toString(),
                            type: "ellipse",
                            icon:"ellipse",
                            active: false,
                            name: ["category_news"],
                            src: "/plus/category_news",
                            role: [0, 1]
                        },
                        {
                            title: t("menu.default.list_news").toString(),
                            type: "ellipse",
                            icon:"ellipse",
                            active: false,
                            name: ["list-post"],
                            src: "/plus/list_news",
    
                            role: [0, 1]
                        },
                    ]
                },
                {
                    title: t("menu.default.qa"),
                    src: "/qa/list",
                    icon:"Q/A",
                    type: "Q/A",
                    name: "Q/A",
                    active: true,
                    role: [0]
                },
              
                {
                    title: t("menu.default.feedback"),
                    src: "/feedBack",
                    icon:"feedback",
                    type: "feedback",
                    name: "Feedback",
                    active: true,
                    role: [0]
                },
               
                {
                    title: t("menu.default.recruitment"),
                    src: "/recruitment/list",
                    type: "recruitment",
                    icon:"recruitment",
                    name: "VipManagement",
                    active: true,
                    role: [0]
                },
                {
                    title: t("menu.default.info_customer"),
                    src: "/info_customer",
                    type: "info_customer",
                    icon:"info_customer",
                    name: "VipManagement",
                    active: true,
                    role: [0]
                }
            ];
        },
    },
});
