import {defineStore} from 'pinia';

interface Loading {
    isLoading: boolean;
}

export const loadingStore = defineStore('loading', {
    state: (): Loading => {
        return {
            isLoading: false,
        };
    },
    actions: {
        setLoading (status) {
            this.isLoading = status
        }
    },
});

export const loadingGenerateImageStore = defineStore('loadingGenerateImage', {
    state: (): Loading => {
        return {
            isLoading: false
        };
    },
    actions: {
        setLoading (status) {
            this.isLoading = status
        }
    },
});
