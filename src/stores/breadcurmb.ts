import {defineStore} from 'pinia';

interface Breadcrumbs {
    title: string;
    breadcrumbs: any;
    isShowSelect: Boolean;
}

export const breadcrumbsStore = defineStore('breadcrumbs', {
        state: (): Breadcrumbs => {
            return {
                title: '',
                breadcrumbs: [],
                isShowSelect: true,
            };
        },
        actions: {
            setBreadcrumb(title: string, data: any, isShowSelect: boolean = true) {
                this.title = title;
                this.breadcrumbs = data;
                this.isShowSelect = isShowSelect;
            }
            ,
        },
    })
;
