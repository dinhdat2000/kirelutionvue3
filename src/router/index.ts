import { createRouter, createWebHistory } from 'vue-router';
import JwtService from "@/services/jwt.service";
import Login from '../views/pages/Login.vue';
import Home from '../views/pages/index.vue';
import AddCategory from "@/components/service/CategoryForm.vue";
import AddListService from "@/components/service/ListServiceForm.vue"
import AddHastag from "@/components/kireiplus/AddHastagForm.vue"
import AddCategoryNeww from "@/components/kireiplus/AddCategoryNeww.vue"
import AddListNew from "@/components/kireiplus/AddListNews.vue"
import AddQA from "@/components/QA/AddQA.vue"
import AddFeedBack from "@/components/feedback/AddFeedBack.vue"
const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import("../views/pages/index.vue"),
        meta: {layout: 'LayoutDefault', role: [0, 1, 2]}
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
    },
    {
        path: '/introduction',
        name: 'introduction',
        component: () => import("../views/pages/introduce/Introduce.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/ceo',
        name: 'ceo',
        component: () => import("../views/pages/founder/FounderStory.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/whats_kirei',
        name: 'whatskirei',
        component: () => import("../views/pages/what-kirei/WhatKirei.vue"),
        meta: {layout: 'LayoutDefault', }
    },
   
    {
        path: '/service/banner_service',
        name: 'banner_service',
        component: () => import("../views/pages/service/BannerService.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/service/category_service',
        name: 'category_service' ,
        component: () => import("../views/pages/service/CategoryService.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/service/category_service/add',
        name: 'AddCategory',
        component: AddCategory,
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/service/list_service/add',
        name: 'AddListService',
        component: AddListService,
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/service/list_service',
        name: 'list-service',
        component: () => import("../views/pages/service/ListService.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/team_kirei',
        name: 'team_kirei',
        component: () => import("../views/pages/team-kirei/TeamKirei.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/plus/banner_hastag',
        name: 'BannerHastag',
        component: () => import("../views/pages/kirei-plus/BannerHastag.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/plus/banner_hastag/add',
        name: 'AddHastag',
        component: AddHastag ,
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/plus/category_news',
        name: 'category_news',
        component: () => import("../views/pages/kirei-plus/CategoryNeww.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/plus/category_news/add',
        name: 'AddCategoryNeww',
        component: AddCategoryNeww,
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/plus/list_news',
        name: 'list-post',
        component: () => import("../views/pages/kirei-plus/ListPosts.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/plus/list_news/add',
        name: 'AddListNew',
        component: AddListNew,
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/qa/list',
        name: 'Q/A',
        component: () => import("../views/pages/qa/QA.vue"),
        meta: {layout: 'LayoutDefault', }
    },
    {
        path: '/qa/list/add',
        name: 'AddQA',
        component: AddQA,
        meta: {layout: 'LayoutDefault', }
    },{
        path: '/feedBack',
        name: 'Feedback',
        component: () => import("../views/pages/feedback/Feedback.vue"),
        meta: {layout: 'LayoutDefault', }
    },{
        path: '/feedBack/add',
        name: 'AddFeedBack',
        component: AddFeedBack ,
        meta: {layout: 'LayoutDefault', }
    },

];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
