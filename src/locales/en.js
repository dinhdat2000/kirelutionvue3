
export default {
  back: 'Quay Về',
  btn_add: 'Thêm',
  confirm_del: 'Bạn có chắc bạn muốn xóa mục này ?',
  btn_del: 'Xóa',
  btn_search: 'Search',
  seo :'Hỗ trợ SEO',
  btn_seo: 'CHECK SEO',
  no_permisson: 'Bạn không thể truy cập màn hình này.',
  select_all: 'Chọn tất cả',
  export_excel: 'Xuất báo cáo',
  log_out: 'Đăng xuất',
  change_pw: 'Đổi mật khẩu',
  choose_all: 'Chọn tất cả',
  auth: {
    placeholder_email: 'Nhập email',
    placeholder_password: 'Nhập mật khẩu',
    email: 'Email',
    password: 'Mật khẩu',
    forgot_password: 'Quên mật khẩu?',
    login: {
      title: 'Đăng Nhập',
      btn_submit: 'Đăng Nhập',
    },
    forgot_password_page: {
      title: ' Cập nhật mật khẩu',
      new_password: 'Mật khẩu mới',
      placeholder_new_password: 'Nhập mật khẩu mới',
      confirm_password: 'Xác nhận lại mật khẩu mới',
      placeholder_confirm_password: 'Nhập mật khẩu xác nhận',
      btn_submit: 'Cập nhật',
      forgot_password_title: 'Quên mật khẩu',
      btn_send: 'Gửi',
    },
    confirm_email: {
      title: 'XÁC NHẬN EMAIL',
      btn_submit: 'Ok',
      content1: 'Vui lòng kiểm tra email',
      content2: 'và làm theo hướng dẫn',
    },
  },
  dashboard: {
    title: 'Trang chủ',
    copy: {
      btn_copy: 'Copy',
    },
    schedule_list: {
      table: {
        action: 'Actions',
      },
    },
  },
  btn_cancel: 'Hủy',
  btn_submit: 'Lưu',

  menu: {
    default: {
      dashboard: 'Trang chủ',
      introduction: 'Giới thiệu',
      what_kire: 'What’s KIREI',
      ceo: 'Câu chuyện người sáng lập',
      service: 'Dịch vụ',
      team_kire: 'Đội ngũ KIREI',
      kirei_plus: 'KIREI Plus',
      qa: 'Q/A',
      feedback: 'Feedback',
      recruitment: 'Tuyển dụng',
      info_customer: 'Thông tin khách hàng',
      banner_service: 'Banner dịch vụ',
      list_service: 'Danh sách dịch vụ',
      banner_hastag: 'Banner/Hastag',
      list_news: 'Danh sách bài viết',
      hastag :'Hastag',
     
    },
  },

  add :{
    add_hastag: 'Thêm Hastag',
    add_QA : 'Thêm Q/A',
    add_feedback: 'Thêm Feedback',

  },

  toast: {
    update: 'Cập nhật thành công!',
    update_fail: 'Cập nhật thất bại!',
    save: 'Tạo mới thành công!',
    save_fail: 'Tạo mới thất bại!',
    delete: 'Xóa thành công!',
    delete_fail: 'Xóa thất bại!',
  },
  banner :{
    btn_add_service: 'Thêm ảnh banner dịch vụ',
    btn_add :'Thêm ảnh banner',

  },
  validate: {
    required: 'Vui lòng điền {filed}!',
  },
  home :{
    kire_bene:'KIREI’s Benefit',
    kire_banner: 'Banner',
    team_kirei : 'Thành viên'
  },

  post: {
    title: 'tên bài viết',
    seo_title: 'seo title',
    seo_keywords: 'seo keywords',
    seo_description: 'seo description',
    slug: 'đường dẫn',
    breadcrumb_title: 'Bài viết',
    btn_add : 'Thêm bài viết'
  },
 
  category: {
    category_service: 'Category dịch vụ',
    category_add: 'Thêm Category',
    category_news : 'Category tin tức',
  },
 service :{
  btn_add :'Thêm Dịch Vụ',
  before_after:'Ảnh Before/After'

 },
 image :{
  upload :'Tải ảnh lên',

 },
  no_data: 'Hiện chưa có {field} nào. Ấn vào nút "Thêm" để thêm mới {field}',
  result_search: 'Không tìm thấy kết quả phù hợp!',
}
