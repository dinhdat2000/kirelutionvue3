const PROJECT_ID = "projectIDDefault";


export const getProjectIDDefault = () => {
    return sessionStorage.getItem(PROJECT_ID);
};

export const saveProjectIDDefault = id => {
    sessionStorage.removeItem(PROJECT_ID);
    sessionStorage.setItem(PROJECT_ID, id);
};

export const destroyProjectIDDefault = () => {
    sessionStorage.removeItem(PROJECT_ID)
};

export default {
    getProjectIDDefault,
    saveProjectIDDefault,
    destroyProjectIDDefault,
};
