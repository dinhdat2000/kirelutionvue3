import './assets/css/main.css'

import {createApp} from 'vue'
import {createPinia} from 'pinia'

// @ts-ignore
import App from './App.vue'
import router from './router'

import i18n from '@/services/i18n.js'

const app = createApp(App)
app.use(i18n)

// app.use(i18n)
app.use(createPinia())
app.use(router)

app.mount('#app')
